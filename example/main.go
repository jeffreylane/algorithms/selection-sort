package main

import (
	"flag"
	"fmt"

	ss "gitlab.com/jeffreylane/algorithms/selection-sort"
)

var verbose *bool = flag.Bool("verbose", false, "print the number of operations")

func main() {
	flag.Parse()
	ss.Verbose = *verbose
	// does a simple selection sort using a passed sort direction func moreOrLess
	scrambled := []int{9, 1, 10, 3, 7, 5, 8, 4, 2, 6}
	asc := func(a, b int) bool {
		return a > b
	}
	desc := func(a, b int) bool {
		return a < b
	}

	fmt.Printf("before sort: %v\n", scrambled)
	ascending := ss.SelectionSort(scrambled, asc)
	fmt.Printf("asc sort: %v\n", ascending)
	descending := ss.SelectionSort(scrambled, desc)
	fmt.Printf("desc sort: %v\n", descending)

}
