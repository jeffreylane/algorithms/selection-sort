package ss

import "fmt"

var (
	Verbose bool
	Ops     int
)

// TODO: Research other selection sort algs.  The number of ops doesn't make sense to me yet.
// Does a selection sort on a using supplied comparer func.  For example,
// if you want to sort a slice of ints asc or desc, you would
// pass a func args for the comparer parameter that might look like this:
/*

	asc := func(a, b int) bool {
		return a > b
	}

	desc := func(a, b int) bool {
		return a < b
	}
*/
func SelectionSort[A ~[]E, E any](a A, comparer func(E, E) bool) A {
	for i := 0; i < len(a); i++ {
		if i == len(a)-1 {
			break
		}
		for j := i + 1; j < len(a); j++ {
			Ops++
			if comparer(a[i], a[j]) {
				a = swap(a, i, j)
			}
		}

	}
	if Verbose {
		fmt.Printf("number of operations: %v\n", Ops)
		Ops = 0 //reset
	}
	return a
}

func swap[A ~[]E, E any](a A, x, y int) A {
	Ops++
	tmp := a[x]
	a[x] = a[y]
	a[y] = tmp
	return a
}
